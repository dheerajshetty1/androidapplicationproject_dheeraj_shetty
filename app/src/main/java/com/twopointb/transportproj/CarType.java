package com.twopointb.transportproj;

public enum CarType {
    SEDAN,
    RACE_CAR
}
